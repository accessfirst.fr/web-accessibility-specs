# Web accessibility specs

Specs and instructions to develop accessible web content and UI

[[_TOC_]]

## Hidden content

*_The appropriate technique MUST be used to hide content._*

### Screen readers and CSS

With the CSS technology, it is possible to modify the visual aspect (the presentation layer) of an HTML document (the content layer). In particular, it is possible to:

1. Remove content from within the presentation layer, although it was in the content layer
1. Hide content in the presentation layer, while leaving it available in the content layer
1. Add content within the presentation layer, although it was not in the content layer

Example of possibility 1: the CSS directive `display:none` removes the affected element and its descendants from the flow, and the presentation layer. However it can be recalled by changing the display property to block for example.

Example of possibility 2: the CSS directive `position:absolute;left:-100000px` positions the affected element outside of the viewport, but it’s still available to user agents that do not take this directive into account.

Example of possibility 3: the CSS directive content can generate text content in elements or pseudo-elements (`::after`, `::before`).

The way screen readers deal with in-browser content has some consequences:

1. Content removed from flow is not output by screen readers
1. Content that is visually hidden, by positioning it outside of the viewport, is output by screen readers
1. For content generated from the presentation layer: some screen readers output it, others don’t

The latter one implies that some precautions must be taken with CSS-generated content, in particular with icon fonts.

These behaviors allow to:

1. hide content to all users
1. hide content to all but screen reader users
1. hide content to screen reader users only

Example of use case for 1: a submenu is hidden until the user lands on a menu item, making it appear on screen.

Example of use case for 2: an information is conveyed through the visual aspect of content (like an icon in a graphic button), and made available as text to screen reader users only.

Example of use case for 3: some content like ASCII art makes sense only if perceived visually, while being meaningless if output vocally.

### Hiding content to all users

####	Using CSS properties

These CSS properties are known to remove an element from flow. And therefore, it is hidden to all user agents, including screen readers:

*	`display:none` (nulled by affecting any other value, like `block`, `inline`, etc.)
*	`visibility:hidden` 
*	`clip:0`
*	`font-size:0`
*	`width:0`, `height:0`, on block elements

If the content is supposed to be available to screen readers (like a field label or a table caption), this is *not* the right hiding technique.

####	Using the `hidden` attribute

The `hidden` attribute can be affected to any flow element.

Example:

```html
  <p hidden>Don’t show me.</p>
```

Example:
```html
<p hidden="true">Don’t show me.</p>
```

Its effect is similar to `display:none`, i.e. the element is removed from flow. And therefore, it is hidden to all user agents, including screen readers. However, if the `display` property is set to block, for instance, it will be displayed, regardless of the current value of `hidden`. It is recommended to make the value of `display` and `hidden` match, to avoid side effects.

The ARIA specifications describe the `aria-hidden` attribute, that hides content to assistive technologies (i.e. screen readers) only.
It is strongly advised to make match the value of `hidden` and the value of `aria-hidden`:

*	elements with `hidden` or `hidden=true` should have `aria-hidden=true`
*	elements with `hidden` not declared or `hidden=false` should have `aria-hidden=false` (or not defined)

However there are situations where a visible content may have `aria-hidden=true`.

###	Hiding content from view, not to screen readers

####	CSS techniques

There are two common techniques, following these general principles:

*	To position the content outside of the browser visible area (the viewport);
*	To display it as a single pixel.

Different browser behaviors have led to refine these techniques to make them more robust across browsers. Currently, we advise to apply the one proposed here: https://hugogiraudel.com/2016/10/13/css-hide-and-seek/ 

```css
.visually-hidden {
  border: 0 !important;
  clip: rect(1px, 1px, 1px, 1px) !important;
  -webkit-clip-path: inset(50%) !important;
          clip-path: inset(50%) !important;
  height: 1px !important;
  overflow: hidden !important;
  padding: 0 !important;
  position: absolute !important;
  width: 1px !important;
  white-space: nowrap !important;
}
```

####	Hidden content and tab order

Non-interactive content that is visually hidden, must not be in tab order.

When a visually hidden content receives focus, it must become visible.

####	Case of interactive elements

Sometimes, interactive elements (like links or form fields) are inside pieces of content that are visually hidden. A typical use case is a skip link. When they receive focus, they must be made visible. If the visually-hidden class above is used as the hiding technique, the solution is to reset it:

```css
.visually-hidden-focusable:focus,
.visually-hidden-focusable:active {
  clip: auto !important;
  -webkit-clip-path: none !important;
          clip-path: none !important;
  height: auto !important;
  overflow: visible !important;
  width: auto !important;
  white-space: normal !important;
}
```

###	Hiding content to screen readers only

The `aria-hidden` attribute hides content from screen readers when set to true. It does not affect the visual rendering: elements with `aria-hidden="true"` are still visible by default.

Example:

```html
<p aria-hidden=true>You can see me, but screen readers ignore me.</p>
```

Note : the value of `aria-hidden` must be explicitly set to `true` (any other value is equivalent to `false`).

According to specifications, `aria-hidden=false` should override the `hidden` attribute, but this is not well supported, so it cannot be used reliably to provide content to screen readers only.

A typical use case for `aria-hidden` is font icons. See "Font icons" section below for details on their accessible implementation. 

## Font icons

*_The appropriate technique to display icon fonts MUST be used._*

Icon fonts are mainly composed of graphic symbols. They are very handy to display simple symbols: they can be easily resized, like any font-based character, and are very light-weight, once the font has been downloaded and stored in the browser cache. A common implementation is to generate them with a CSS content directive, allowing to have a seemingly empty element when CSS or the font are not supported.

Example with FontAwesome:

```html
<i class="fa fa-facebook"></i>
```

With the associated CSS:

```css
.fa-facebook-f::before, .fa-facebook::before {
    content: "\f09a";
}
.fa {
    font: normal normal normal 14px/1 FontAwesome;
(…)
}
```

Which results in displaying a Facebook icon.

However, some screen readers will render the generated content, which will sound meaningless. On the other hand, it will be necessary to provide a textual equivalent to convey the same information. The basic principle is to provide visually hidden text, and hide the icon to screen readers.

Example:

```html
<span class="visually-hidden">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i>
```

## Standard interactive components



An interactive component is a piece of the interface that can receive user input: a click, a selection in a list, a text entry…

Two categories of interactive components will be considered in this document:

* Standard interactive components (current page): components that corresponds to core HTML tags. Examples: button, link, interactive form element.
* Custom interactive components (aka widgets): HTML-based components that perform functions through additional scripting. Examples: drop-down menu, tab system, pop-up dialog box.
The subject of custom components is a broad one, that will be addressed on a case-by-case basis.

Only standard components are addressed in the current section. This includes:

* Buttons
* Links
* Interactive form elements

Standard components like `<map>`, `<audio>` and `<video>` provide a sophisticated UI with several inner components. Since they are handled by the browser, with little to no control from the author, they are not addressed in this document.

Before going into details about each of the categories above, some key concepts are explained below.

### Accessible name

_*An interactive component MUST have an accessible name.*_

The accessible name is an information provided by an application to assistive technologies, through the accessibility tree. It allows the user to identify the component, and know its purpose.

In the case of web content, the browser calculates the accessible name by applying parsing rules on the generated source code. Depending on the element, it can be the content of its text node, of its descendants, of its attributes, or a combination of those.

There are several methods to define the accessible name of an interactive component:

* based on the HTML content, or associated content, of the element
* based on attributes of the element, like `title` or `value`
* based on the `aria-label` property
* based on the `aria-labelledby` property

If more than one methods are used on the same interactive component, the priority rule is:

`aria-labelledby` > `aria-label` > calculated from DOM (HTML or attribute)

It means that if an interactive component has both a text node and an `aria-label`, it’s the value of `aria-label` that will be considered as its accessible name. In the case of a text node competing with an attribute like `title`, the calculation may vary according to the component and the browser. It is therefore recommended to avoid using both methods simultaneously.

#### Based on the HTML content

The accessible name of an interactive component will generally be the text node, or the text node of a descendant.

In the following examples, all buttons have "Menu" as their accessible name.

Example 1:

```html
<button type="button">Menu</button>
```

Example 2:

```html
<button type="button"><span>Menu</span></button>
```

Example 3:

```html
<button type="button"><img alt="Menu" src="menu-icon.png" /></button>
```

#### Based on an attribute

The attribute used as an accessible name of an interactive component will generally be the `title` attribute. Although it is available to assistive technologies, and visible on mouse hover, it is not as reliable as a text node:

* its rendering in screen readers depends partly on user settings
* it is (generally) not displayed on keyboard focus

So it should be only used in specific cases, like providing additional, non-critical information. Its appropriate use for links or fields is detailed further in this document.

Example:

```html
<button type="button" title="Menu (press Shift + M to open)">Menu</button>
```

Some interactive component take the `value` attribute, which can be used as the accessible name.

Example:

```html
<input type="submit" value="Send" />
```

#### The `aria-label` property

The `aria-label` property of the interactive component will be considered as its accessible name by assistive technologies.

Example:

```html
<button aria-label="Menu" type="button">
    <img alt src="menu-icon.png" />
</button>
```

This is handy when there is no room for a visible text in the interface. However:

* it is not visually rendered, so it should be used only when the inner or adjacent contents make the purpose of the interactive component clear enough, for users who don’t have access to the information in the `aria-label` attribute
* it can’t contain HTML tags, which could be required in some cases (eg. to indicate changes of language).

#### The `aria-labelledby` property

The `aria-labelledby` property of the element will associate it to a passage of text, through its `id`. The passage of text will be considered as the interactive component’s accessible name by assistive technologies.

Example:

```html
<button aria-labelledby="text_id" type="button">
    <img alt src="menu-icon.png" />
</button>
<span id="text_id">Menu</span>
```

The `aria-labelledby` property can be used to compose the accessible name with several sources, by referencing the different passages of texts through their `id` attributes, separated by whitespaces.

Example: The accessible name of this button will be "Delete Annual report"

```html
<span id="file_1">Annual report</span>
<button aria-labelledby="delete_icon file_1" type="button">
    <img id="delete_icon" alt="Delete" src="trash.png" />
</button>
```

The same passage of text can be used to label several interactive components.

Example:

```html
<span id="file_1">Annual report</span>
<button aria-labelledby="delete_icon file_1" type="button">
    <img id="delete_icon" alt="Delete" src="trash.png" />
</button>

<button aria-labelledby="archive_icon file_1" type="button">
    <img id="archive_icon" alt="Archive" src="archive.png" />
</button>

```

The `aria-labelledby` property must be used only for visible contents. If it’s not visible by default, it must become visible when the interactive component gets focus. When the content is not visible, use `aria-label` instead.

#### Relevance of the accessible name

_*An interactive component MUST have a relevant accessible name.*_

An accessible name for an interactive component is relevant if the user can anticipate the effect of activating the component. If a component’s purpose changes after page load, its accessible name must change accordingly. For instance, if a toggle button allows switching between high-contrast mode, and standard colors, then an appropriate accessible name could be "Higher contrast" when standard colors are on, and "Standard colors" when high contrast mode is on.

#### Label in name

_*The visible label of an interactive component MUST be included in its accessible name.*_

Some users need the accessible name of interactive elements, which should be inferred from looking at the interface. A typical use case is voice control software. The user will indicate which element they want to interact with, by issuing vocal commands like "click on [text link]". If the accessible name, known by the assistive technology, differs from what the users sees in the UI, then this approach won't be possible.

At least, the visible label (any text content that is visually associated to the element, and that can reasonably be considered as its label), must be contained in the actual accessible name (retrieved by the assistive technology through the accessibility API). Ideally they are identical. If not, it is considered as a good practice that the accessible name starts with the visible label.

### Accessible role

_*An interactive component MUST have a relevant accessible role.*_

The accessible role is an information provided by an application to assistive technologies, through the accessibility tree. It allows the user to know the component type, and how to operate it.

In the case of web content, the browser calculates the accessible role:

* either by fetching its implicit (or native) role, based on the HTML element type
* or by fetching its explicit role, which is the content of the `role` attribute (if set).

The explicit role overrides the native role, if set. However, it is very important to understand that this role has no other impact than affecting the accessibility tree:

* it doesn’t change the visual design of the component
* it doesn’t change its behaviors
* it doesn’t change its keyboard nor mouse/touch interactions

For example, setting `role=button` on a `DIV` element has no visible effect. Users of assistive technologies will get the information that the current element has the role of a button. But without additional styling and scripting, it will look, behave and interact exactly like a `DIV`, for all users.

An element can only have one role. Therefore, with a construction like `<li role=button>`, users of assistive technologies will be informed that the element is a `button`, but it will not be perceived as a `listitem` anymore, thus losing the associated features from the assistive technology standpoint.

#### Rules for role assignment

The following writing rules shall be followed:

* The native role of an element MUST NOT be changed if it already has a semantic value
* Native elements MUST be used, instead of elements with modified roles
* The explicit role SHOULD NOT be redundant with the native role of an element

An accessible role for an interactive component is relevant if the user can correctly deduce the characteristics of the component, including its reaction to keyboard interactions.

Standard components automatically have the relevant role. Custom components can have a variety of roles, some having no equivalent in HTML. This subject will be addressed on a case-by-case basis.

#### Links vs. buttons

Links and buttons, in HTML, share common features. Among others, they can be clicked, which triggers the click event that is attached to them. With both elements, those events can be used to execute JavaScript instructions, and can be styled to the same extent. Therefore, it is fairly common to meet both elements to serve either function. But they also bear differences, which make them not a true replacement for each other, without extensive extra coding. For accessibility, the main differences lie in the accessible properties (role and properties) and default keyboard behaviours (a link can be clicked with the Enter key only; a button with both Enter or Space). This makes little difference for most users, but users of assistive technologies like screen readers have access to information like the role of the elements. Based on this, they have certain expectations about these elements, like the keyboard interactions, the contextual menu, the focus management, etc. Failing these expectations can cause issues to users with accessibility needs.

To avoid this situation, it is important to choose the right element for the right function:

* links are supposed to download resources (pages, medias, documents...) or set the focus on an element inside the page
* buttons are not; they are suitable for supporting user interactions with the interface, without downloading new resources

Examples of appropriate uses for links:

* navigation links
* anchors (aka in-page links, to "jump" to the main content, for instance)

Examples of appropriate uses for buttons:

* form submission or reset
* clickable areas that alter the display status of widgets, like menus, tabs, or slideshows


Using `<a href>` for links, and `<button>` or `<input type=button|submit|reset>` for buttons, in HTML, lets the browser handle all the heavy-lifting. It is technically possible to use `role=link` and `role=button` (respectively) on elements with no pre-defined role, like SPAN and DIV, but _all_ the behaviours (and there are many) must be added through scripting, in order to make them match all those elements properties, the obvious ones, and the subtle ones alike. Therefore, it is strongly advised to stick with the standard elements, whenever possible.

There are cases where the choice between a link or a button can be balanced. For example, when the actionable element opens a modal dialog (aka pop-in). Technically it is a part of the current page that is displayed in a way that makes it "feel" like a different window. So it should be a button. But since it behaves like a new page, until it's dismissed, it could make sense to associate it with a link. Single Page Applications, where there's only one page, refreshed with new content upon user interaction, should contain only buttons, following this logic. But from a user point of view, elements that resemble navigation items are expected to be links. Here too, using links makes sense if a link-like behaviour is expectable.

### Accessible description

The accessible description complements the information already available through the accessible role and name. It is not mandatory. It is recommended if the role and name alone may not be sufficient to understand the purpose and properties of an element. It is to be avoided if it adds redundant information, with no clear user value.

Accessible descriptions are particularly useful to distinguish several elements that have the same accessible names. Example: in a documents management page, all Remove buttons have the same accessible name ("Remove"). In order to differentiate each of them, and allow the user to understand which document is associated to which button, the accessible description of each button may be used. It could include the document name or reference, for instance.

It can be set through these means:

* the `title` attribute of links, buttons or input fields. Note that if the accessible name is not set, the `title` attribute will be used as the accessible name instead
* the `aria-describedby` attribute can be used to associate a visible text to an element, and turn it into its accessible description. The mechanism is as follows:
** the element bearing the description has a unique `id` attribute
** the element described has an `aria-describedby` attribute whose value is the `id` of the descriptor.
    
In the following example, the button's accessible description is "This is my description":

 ```html
    <p id="desc">This is my description</p>
    <button aria-describedby="desc">I am a button</button>

```

** to use more than one element as descriptors, the `aria-describedby` attribute can accept the list of their `id` attributes, separated by white spaces. They are output in the order they are set.

In the following example, the button's accessible description is "This is my description in two parts":

 ```html
    <p id="desc1">This is my description</p>
    <button aria-describedby="desc1 desc2">I am a button</button>
    <p id="desc2">in two parts</p>

```

To check the value of the accessible description: https://prj.adyax.com/projects/tandem-projects/wiki/Step-by-step_testing_techniques#AD-01-Check-the-accessible-description-of-an-element-with-the-Accessibility-Inspector

**Important**: the `aria-describedby` attribute takes precedence over the `title` attribute. If both are set, it's the `aria-describedby` attribute that sets the accessible description.

### State and properties

_*An interactive component MUST convey its state and properties to the accessibility tree.*_

An interactive component can have different states and properties, depending on its nature. For instance, a button can be disabled, a link can be active, a radio button can be checked, etc.

Standard components automatically convey their state and properties. Custom components can have a variety of properties, sometimes very specific. This subject will be addressed on a case-by-case basis.

### Device independence

_*All interactive components MUST be operable with a keyboard, a touch-based device, or a mouse.*_

To be operable with a mouse, an interactive component must be able to react to a mouse click.

To be operable with a touch-based device, an interactive component must be able to react to a touch event.

To be operable with a keyboard, an interactive component must be able to:

* receive focus through a keyboard only (generally, using the Tab key)
* be activated through a keyboard only (generally, using the Enter, or Space key)

Depending on the component, other keys may be affected to various functions:

* Arrow keys to move focus inside the component
* Esc key to close or dismiss the component
* Page Down, Page Up, Home, End
* modifier keys (like SHIFT or ALT)
* any key, affected to specific functions (like A..Z keys to move focus to the matching items in a selection list)

Standard components all natively support mouse, touch and keyboard. Custom components must be scripted to meet standardized behaviors, and/or users expectations. This subject will be addressed on a case-by-case basis.

### Focus visibility

When navigating with a keyboard, users must be able to know where the focus is. By default, browsers provide this information by drawing an outline around interactive elements that can get focus (referred to as "focusable" hereafter). Focusable elements include links, buttons, input form elements, and elements with a `tabindex` attribute set to 0 or more.

This default behaviour can be overridden, in CSS, through the `outline` property. Each browser has its own focus indicator style: a yellow, thick border for Safari; a blue border with a halo for Chrome; a 1px-thick dotted border for Firefox, the same colour as the text of the element. The Firefox outline is the less visible one, in most situations. It is therefore taken as the reference for the minimal visibility of the focus indicator. The `outline` property must not be degraded nor suppressed (using `0` or `none` for any of its sub-properties).

In some situations, although untouched, the focus indicator may not be visible enough for practical use. In this case, it is advised to reinforce its visibility, through a thicker width, or a more noticeable colour.


## Buttons

_*A button MUST be implemented with the `<button>` or `<input type="button|submit|reset">` tags.*_

Buttons can be used to trigger an action, that will affect the interface. Specialized types of button can submit or reset a form. Except for the latter ones, the action must be scripted.

They have specific properties, and users have expectations about some of their behaviors. This is the reason why substitutes (like links with modified behaviors) are generally not fully satisfying.

Buttons have a native role of `button`. There is no need to set it explicitly.

Buttons support keyboard and mouse natively. There is no need to script for their support.

### `INPUT` of type `BUTTON`

Syntax:

```html
<input type="button" value="A button" />
```

The content of the `value` attribute is displayed on screen, and used by assistive technologies to calculate its accessible name.

This element has no intrinsic behavior, it must be added in JavaScript.

### `INPUT` of type `submit`

Syntax:

```html
<input type="submit" value="Send" />
```

The content of the `value` attribute is displayed on screen, and used by assistive technologies to calculate its accessible name.

The default behavior of this element is to submit the values of the form it is associated with.

### `INPUT` of type `image`

Syntax:

```html
<input type="image" alt="Send" src="right-arrow.gif" />
```

The content of the `alt` attribute is not displayed on screen, but it is used by assistive technologies to calculate its accessible name. If the image used can appear ambiguous to some users, it is recommended to add a `title` attribute, with the same value as `alt`.

The default behavior of this element is to submit the values of the form it is associated with.

### `INPUT` of type `reset`

Syntax:

```html
<input type="reset" value="Reset" />
```

The content of the `value` attribute is displayed on screen, and used by assistive technologies to calculate its accessible name.

The default behavior of this element is to reset fields to their initial values in the form it is associated with.

### `BUTTON`

Syntax:

```html
<button type="button">A button</button>
```

The content between the opening and closing tags is displayed on screen, and used by assistive technologies to calculate its accessible name.

A `BUTTON` element can be of types `button`, `submit`, or `reset`, with the same characteristics as the `INPUT` elements of same types.

Unlike `INPUT`, the content of the button can be HTML (of type phrasing content, and non-interactive), including images. The calculation of the accessible name takes into account the different text nodes and relevant attributes of the included elements.

The default type is `submit`. If the button is used for another function, in or outside of a form, it is recommended to affect the `button` type, to avoid involuntary submission by the user.

## Links

### General case

_*A link MUST be implemented with the `<a>` tag and have a `href` attribute.*_

Links are interactive components that trigger the download of the resource located via the `href` attribute, and move the reading point to the fragment identified via the `#` character in the URL.

Example 1: downloads the resource at ./home.html on the server

```html
<a href="home.html">Home</a>
```

Example 2: moves the reading point to the element with `id="main"`

```html
<a href="#main">Skip to main content</a>
```

Although links can be used to trigger JavaScript code, it is not recommended to use them for this purpose ; use buttons instead. If the author wishes to execute JavaScript code before loading a resource, it is more robust to affect the target URL to the href attribute, and intercept the click event in order to execute the script before it triggers the download. That way, if JavaScript is not supported, the link remains functional.

Links have a native role of `link`. There is no need to set it explicitly.

Links support keyboard and mouse natively. There is no need to script for their support.

Links have states like `active`, `visited`. The `active` state may not be exposed to assistive technologies, so if a link is used as a menu item, and is currently active, this state may be conveyed through the accessible name (see below).

The accessible name of a link can be conveyed through its inner HTML content, the `title` attribute, or the `aria-label` or `aria-labelledby` properties. However, depending on the user settings, a screen reader may ignore, or favor, the `title` attribute. This setting can’t be determined from within the content, so there are specific writing rules for link titles to ensure robustness.

In any case, the text node MUST NOT be empty. If it contains only images, their alternatives MUST NOT be empty.

The accessible name can be used to convey a specific state.

Example: the current page is the Contact page. The "active" state of the corresponding link in the menu is conveyed through the use of the `title` attribute:

```html
<nav>
  <ul>
    <li><a href="home.html">Home</a></li>
    <li><a href="products.html">Products</a></li>
    <li><a href="jobs.html">Jobs</a></li>
    <li><a href="contact.html" title="Contact (current page)">Contact</a></li>
  </ul>
</nav>
```

Note: `<a>` elements may have no `href` attribute, which is known as "anchor links". In this case, they are not in the tabbing sequence, and are meant only to be the target of an hyperlink. This is useful for in-page navigation, for instance. This practice tends to be replaced by the addition of an `id` attribute to the targeted area. Anchor links generally don’t have inner text.

Example:

```html
<a href="#toc">Go to the Table of Content</a>
(…)
<a name="toc" id="toc"></a>
<h2>Table of content</h2>
```

Note: both `name` and `id` attributes are defined for the anchor link, to support legacy browsers.

### Links opening new windows

_*Links that open new windows SHOULD be signalled to the user.*_

A "new window" is open when the target of the link is a different browser window or tab. It becomes the root of a new navigation history.

When a new window is open, some users might be disoriented, because of the change of context, and the break in navigation history (going back to the previous page can’t be done with the "back" feature of the browser). It is necessary, then, to warn them through an explicit message.

Note: Although it is not prohibited from an accessibility standpoint, new windows should be avoided if possible. Research suggests that users prefer keeping control on their navigation patterns, and know how to open a new window when they decide to. Forcing to open a new window lessens their control over their navigation, and generates frustration.

For links, the link text or the link title are appropriate to signal a new window. If a graphic symbol is used, it must have a relevant alternative.

Example 1: through the link text:

```html
<a href="http://anothersite.com" target="_blank">Our partner’s website (new window)</a>
```

Example 2: through the link title:

```html
<a href="http://anothersite.com" target="_blank" title="Our partner’s website (new window)">Our partner’s website</a>
```

Example 3: through a graphic symbol:

```html
<a href="http://anothersite.com" target="_blank">
      Our partner’s website <img src="newpage.png" alt="new window" />
    </a>
```

## Form controls

_*The appropriate type of field SHOULD be used where relevant.*_

The HTML provides a range of form controls that can be interacted with:

* Buttons 
* text field, text area
* selection lists
* checkbox, radio button
* file input
* password
* date, time fields (HTML5)
* number, range fields (HTML5)
* color picker (HTML5)
* URL, e-mail, telephone number fields (HTML5)

The latter types have been introduced by HTML5. For some of them, support is uncertain across browser. Even when supported in the GUI, they may not be satisfyingly supported by assistive technologies, meaning that the accessibility tree is not correctly generated. So in addition to browser support, testing with major screen readers is necessary.

Where supported, form controls have built-in roles, keyboard and mouse support, and state and properties. Their accessible name is their label. See below for different techniques to associate labels to form controls.

## Field labels

_*In forms, a label MUST be programmatically associated to each field.*_

_*In forms, labels MUST be relevant: they describe the purpose of the field in a clear and concise way.*_

When available in different forms, in a same page or set of pages, labels MUST be consistent: they are similar when the fields purposes are similar.

The techniques below are various ways to define a label for a form field. It can be any type of field.

### `<label>` tag

#### Explicit labelling

A `<label>` tag is programmatically associated to a field through the `for`/`id` attributes pairing. This is known as explicit labelling.

This should be the preferred way to label a field:

* it’s the most robust technique, implemented in every browser since HTML 2.0, and supported by all assistive technologies
* it provides a larger click area, since the label can be clicked to set focus on the corresponding field

Example KO (no `<label>`):

```html
<p>Your name</p><input type="text" />
```

Example KO (`<label>` tag with no `for` attribute):

```html
<label>Your name</label><input id="field_id" type="text" />
```

Example KO (`<label>` tag with incorrect `for` / `id` pairing):

```html
<label for="something">Your name</label><input id="something_else" type="text" />
```

Example OK:

```html
<label for="field_id">Your name</label><input id="field_id"/>
```

#### Explicit vs. implicit labeling

The HTML standards consider that wrapping the `<label>` around the `<input/>`  should create a programmatic association between the label and the field. This is known as implicit labelling. Example:

```html
<label>Check if you agree <input type="checkbox" /></label>
```

It is a common practice for checkboxes and radio buttons, because it provides an even larger click area, and saves from defining an `id` for each element. While this has been supported in browsers for a long time, accessibility standards remain unclear on whether this should be considered as reliable enough. It’s safer to use either the explicit labelling technique described above, or combine both.

Example:

```html
<label for="checkbox_id">Check if you agree <input id="checkbox_id" type="checkbox" /></label>
```

### `title` attribute

The `title` attribute of the field will be considered as its accessible name, i.e. its label, by assistive technologies, ONLY if there's no other labelling technique being used. In addition, a tooltip is displayed when the user hovers the mouse cursor over it.

Example:

```html
<input title="Your name" type="text"/>
```

This is handy when there is no room for a visible label in the interface. However:

* the tooltip is visible only to mouse users
* it should be used only when the visual context makes the purpose of the field clear enough, for users who don’t have access to the information in the `title` attribute
* it can’t contain HTML tags, which could be required in some cases.

### `aria-label` property

The `aria-label` property of the field will be considered as its accessible name, i.e. its label, by assistive technologies.

Example:

```html
<input aria-label="Your name" type="text"/>
```

This is handy when there is no room for a visible label in the interface. However:

* it is not visually rendered, so it should be used only when adjacent content makes the purpose of the field clear enough, for users who don’t have access to the information in the `aria-label` attribute
* it can’t contain HTML tags, which could be required in some cases.

### `aria-labelledby` property

The `aria-labelledby` property of the field will associate it to a passage of text, through its `id`. The passage of text will be considered as the field’s accessible name, i.e. its label, by assistive technologies.

Example:

```html
<p id="text_id">Your name</p>
    <input aria-labelledby="text_id" type="text"/>
```

The `aria-labelledby` property can be used for multiple labels, by referencing the different passages of texts through their `id` attributes, separated by whitespaces.

Example: The accessible name of this field will be "Your date of birth Year"

```html
<p id="DOB">Your date of birth</p>
    <p id="y">Year</p>
    <input aria-labelledby="DOB y" type="text"/>
``` 

The same passage of text can be used to label several fields.

Example:

```html
    <p id="search">Search our catalog</p>
    <input aria-labelledby="search" type="text"/>
    (…)
    <input aria-labelledby="search" type="text"/> 
```

The `aria-labelledby` property must be used only for visible contents. If it’s not visible by default, it must become visible when the field gets focus. When the content is not visible, use `aria-label` instead.


### Calculation of accessible name

For input fields, the calculation of accessible name is as follows, by order of precedence:

    `aria-labelledby` > `aria-label` > calculated from DOM (`<label>`) > `title

It means that if a field has both a `<label>` and an `aria-label`, it’s the content of `aria-label` that will be considered as its accessible name/label.

### Non-visible label

In some situations, there is not enough room in the interface to have a visible label. One of the following techniques can be used:

* a `<label>` tag, visually hidden only
* a `title` attribute
* an `aria-label` property

When the label contains additional information that are important to know for the user, such as help for input, then there must be clear visual cues to convey the information that is available in the non-visible label.

Example: on a website where the red border is a visual convention for mandatory fields:

```html
<p>Subscribe to our newsletter</p>
    <input style="border:3px red solid" aria-label="Your e-mail (required)" required />
```

### The `placeholder` attribute

_*In form fields, the `placeholder` attribute MUST NOT be used to provide information that is necessary to perform a task successfully.*_

The `placeholder` attribute can be used in text fields to provide a hint, such as examples of possible input terms.

It is NOT a reliable substitute for labels, and should be overridden by actual labels for the calculation of accessible name. It also holds a number of characteristics that can become drawbacks for accessibility:

* its text colour is generally dimmed, offering a low contrast ratio, which can’t be easily controlled by the user
* when the field has a non-empty `value` attribute, the `placeholder` is not visible anymore

Although actual labels should override the `placeholder` attribute in calculation of the accessible name, some user agents will output it instead of the `title` attribute, when defined. Therefore:

In form fields, if the `title` attribute is used for labelling, then the `placeholder` attribute MUST either be not defined, or have the same value as the `title` attribute.

## Fields grouping

### Use cases for fields grouping

_*In forms, fields for information of same nature MUST be grouped.*_

In this context, fields are of "same nature" when the expected inputs will constitute one piece of data that should be considered as a whole. Examples:

* day, month, year, for a date
* regional prefix and main number, for a phone number
* X, Y and Z axis positions for space coordinates
* postal address block, when several address blocks are to be filled in the same page
* groups of radio buttons or checkboxes tied together
* In general, the design makes the grouping obvious through visual presentation. This grouping must be programmatically determined for non-visual users, and users who display the interface with a different design.

### Techniques for fields grouping

#### With `<fieldset>` / `<legend>`

`<fieldset>` is the HTML structure designed to define field groups. It is wrapped around the considered set of fields.

`<legend>` is the child tag of `<fieldset>` that labels it. It must be the first descendant of `<fieldset>`.

The expected behaviour from screen readers is to recall the content of `<legend>` along with the label of each field. This way, the user knows which particular "month" field currently has focus, which is especially useful when several date fields are available in the form or page.

Example:

```html
<fieldset>
    <legend>Date of birth</legend>
<label for="dobd">Day</label>
    <input size="2" id="dobd" type="text"  />
 <label for="dobm">Month</label>
    <input size="2" id="dobm" type="text"  />
<label for="doby">Year</label>
   <input size="4" id="doby" type="text"  />
  </fieldset>
```

See this Example of grouped date fields in this CodePen.

#### With `role=group`

Fields can be grouped accessibly through the use of the `group` role on a container, and a title can be assigned with `aria-label` (appropriate if the title is not visible) or `aria-labelledby` (appropriate if the title is visible).

Example with `aria-label`:

```html
<div role=group aria-label="Date of birth">
<label for="dobd">Day</label>
    <input size="2" id="dobd" type="text"  />
 <label for="dobm">Month</label>
    <input size="2" id="dobm" type="text"  />
<label for="doby">Year</label>
   <input size="4" id="doby" type="text"  />
</div>
```

Example with `aria-labelledby`:

```html
<p id="group_label">Date of birth</p>
<div role=group aria-labelledby="group_label">
<label for="dobd">Day</label>
    <input size="2" id="dobd" type="text"  />
 <label for="dobm">Month</label>
    <input size="2" id="dobm" type="text"  />
<label for="doby">Year</label>
   <input size="4" id="doby" type="text"  />
</div>
```

#### Through labelling

Fields can be grouped accessibly through labelling, i.e. when their relationship can be deducted from their labels.

Example:

```html
<label for="dobd">Date of birth: Day</label>
    <input size="2" id="dobd" type="text"  />
 <label for="dobm">Date of birth: Month</label>
    <input size="2" id="dobm" type="text"  />
<label for="doby">Date of birth: Year</label>
   <input size="4" id="doby" type="text"  />
```

## Input assistance

_*In forms, input assistance MUST be provided to the user before submission.*_

_*Before submission, the user MUST be able to know:*_

* if a field is required
* which are the accepted input formats, if any, for required fields

_*Before submission, the user SHOULD be able to know:*_

* which are the accepted input formats, if any, for non-required fields

### Required fields

Input fields can be programmatically defined as required with:

* the `required` attribute
* the `aria-required=true` property

For better support, both must be set.

Example:

```html
<label for="name">Your name:</label><input id="name" required aria-required="true"/>
```

However there are caveats:

* they are not fully supported yet, across all user configurations
* the `aria-required `property is available to assistive technologies only
* where they are not supported, there is no visual indication, and the browser behaviour must be scripted
* they can’t be used on groups of fields where filling at least one field is required to make the group pass validation

Even when `required` or `aria-required` are set, indicate mandatory fields through one of these means (by order of preference, descending):

* in full words, in the label. Example:
```html
<label for="name">Your name (mandatory)</label>
```
* with an image that represents a conventional symbol for mandatory fields, in the `<label>`. Example:
```html
<label for="name">Your name <img src="image-of-star.gif" alt="mandatory" title="mandatory"/></label>
```
* with a conventional character, like "*". In which case, a text above the field must inform the user of the meaning of this character. 


Characters in the message and in the label should look the same. Example:
```html
<p>Fields with <span class="required">*</span> are mandatory</p>
(…)
<label for="name">Your name <span class="required">*</span></label>
```

### Groups of fields, at least one required

Examples of use cases:

* among a group of checkboxes, the user must check at least one box
* among a group of radio buttons, the user must check at least one button
* among a group of text fields, the user must fill at least one field

In such cases, the `required` and `aria-required` properties can’t be used, because they apply to one given field, not to a group of fields.

The only way, in HTML, is to group the fields through the appropriate means, and provide the information via the label of the group.

#### With `<fieldset>` / `<legend>`

The information shall be indicated through the `<legend>`.

Example :

```html
<fieldset>
    <legend>Pick your flavors (at least 1)</legend>
<label for="flavor_1">
    <input id="flavor_1" type="checkbox" />Vanilla
</label>
<label for="flavor_2">
    <input id="flavor_2" type="checkbox" />Chocolate
</label>
<label for="flavor_3">
    <input id="flavor_3" type="checkbox" />Tabasco
</label>
</fieldset>
```

#### With `role=group`

The information shall be indicated through the `aria-label` or the passage of text referenced via `aria-labelledby`.

Example with `aria-label`:

```html
<div role=group aria-label="Pick your flavors (at least 1)">
<label for="flavor_1">
    <input id="flavor_1" type="checkbox" />Vanilla
</label>
<label for="flavor_2">
    <input id="flavor_2" type="checkbox" />Chocolate
</label>
<label for="flavor_3">
    <input id="flavor_3" type="checkbox" />Tabasco
</label>
</div>
```

Example with `aria-labelledby`:

```html
<p id="group_label">Pick your flavors (at least 1)</p>
<div role=group aria-labelledby="group_label">
<label for="flavor_1">
    <input id="flavor_1" type="checkbox" />Vanilla
</label>
<label for="flavor_2">
    <input id="flavor_2" type="checkbox" />Chocolate
</label>
<label for="flavor_3">
    <input id="flavor_3" type="checkbox" />Tabasco
</label>
</div>
```

#### Through labelling

The information shall be indicated through labelling. It should be noted that even though it is technically possible, it could be hard to find the appropriate wording.

```html
<label for="flavor_1">
    <input id="flavor_1" type="checkbox" />Pick your flavors (at least 1): Vanilla
</label>
<label for="flavor_2">
    <input id="flavor_2" type="checkbox" />Pick your flavors (at least 1): Chocolate
</label>
<label for="flavor_3">
    <input id="flavor_3" type="checkbox" />Pick your flavors (at least 1): Tabasco
</label>
```

### Expected input formats

Form fields (`<input/>` tag) can have different types, like `email`, `url`, `date`, `number`, etc. If the browser supports a given type, the UI will manage both input masking and error management. It will also display the adapted on-screen keyboard where appropriate.

Whenever the expected data matches a type of input, it should be used. But not all types are equally supported across all browsers. So, the following strategy should be adopted:

* check browser support for each type or attribute (through a library like Modernizr)
* if not supported, provide a fallback to achieve similar behaviour, if needed

The most important goal here is to make the corresponding action achievable by the user. In some cases, the fallback may be provided by the browser by default: if it doesn’t recognise the type (like `date` for instance), it displays the field as the default type (text). A sophisticated fallback, like a JavaScript date picker, isn’t strictly necessary from an accessibility standpoint, because the user still has the ability to type in a date manually. However, it may be desirable to provide a user interface to make the process easier for the user (in the case of a date picker, by excluding invalid dates, and setting the right date format automatically). In any case, this UI must be fully accessible, including support for assistive technologies and keyboard navigation.

If the input must follow certain format rules (like yy/mm/dd for a date), and the corresponding type is not supported, or there is no such type, then the expected format MUST be provided to the user for required fields, and SHOULD be provided for non-required fields.

One of these techniques may be used (by order of preference, descending):

* through the label. Example:
```html
<label for="dob">Date of birth (yyyy/mm/dd)</label><input id="dob" type=text />
```
* using the `aria-describedby` attribute. Example:
```html
<label for="dob">Date of birth</label>
<input id="dob" aria-describedby="help-format-date" type=text />
<p id="help-format-date">Expected format: yyyy/mm/dd</p>
```
* through an explicit text, provided before the field(s). Example:
```html
<p>Expected format for dates: yyyy/mm/dd</p>
(…)
<label for="dob">Date of birth</label><input id="dob" type=text />
```

## Form error messages management

_*When an input error occurs in a form, the system MUST help the user locate the error and fix it.*_

### Insertion of error messages in the form

The input type can be used to determine the type of data that is expected by the form, for standardised formats. In this case, the content, location and behaviour of the message are handled by the browser.

For specific formats or input masks, or when the type is not supported, the author must provide the error messages, and a display strategy.

Error messages can be displayed:

* as a summary, before the form
* next to each field in error, and programmatically associated to each field
* both in a summary before the form and next to the field

#### Summary of errors

The error messages can be provided as a summary of all errors detected in the form. Design rules to apply :

* MUST be located before the form, visually and in code
* MUST be clearly identifiable as the summary of error messages; for example, through the use of an explicit heading
* SHOULD be visually outstanding, but not overwhelming

The appreciation of the latter rule will be highly dependent on the form design, user base, and user research.

#### Programmatic association between field in error and message

Locating the message next to the field is not sufficient. Users who don’t see the screen, or who have a different layout to accommodate their needs, may not be able to identify the association conveyed by the visual design.

The following means can be used to this end:

* Through labelling. Example:
```html
<label for="email">Your e-mail: invalid format</label>
```
* Through the `aria-describedby` attribute. Example:
```html
<label for="email">Your e-mail </label>
<input id="email" aria-describedby="error-email" />
<span id="error-email">invalid format</span>
```
* Through the `aria-invalid` attribute. In this case, because it is conveyed only to assistive technologies, there must be a visible cue. Example:
```html
<label for="email">Your e-mail </label>
<input id="email" aria-invalid="true" />
<span>invalid format</span>
```

#### Case of grouped fields

When there's a `<fieldset>` with error management (e.g.: one field must be filled, at least; or one radio button must be checked, at least), the most robust way to inform screen readers in a timely manner is to add the error message to the `<legend>` content.

This is easy to do when the error message is displayed next to the legend (situation A).

However, it can be difficult if the message is not in the visual vicinity of the legend (situation B). In this case, the trick is to have 2 messages: one for screen readers only, in the legend; and one in the display, for sighted users, but hidden to screen readers to avoid repetition.

##### Proposed implementation for solution A

Before submission:

```html
<fieldset>
    <legend>Are you OK?</legend>
    <label><input type=radio />Yes</label>
    <label><input type=radio />No</label>
<fieldset>
```

After submission:

```html
<fieldset>
    <legend>Are you OK? <span class="error">(Please answer the question)</span></legend>
    <label><input type=radio />Yes</label>
    <label><input type=radio />No</label>
<fieldset>
```


##### Proposed implementation for solution B

Before submission:

```html
<fieldset>
    <legend>Are you OK?</legend>
    <label><input type=radio />Yes</label>
    <label><input type=radio />No</label>
<fieldset>
```

After submission:

```html
<fieldset>
    <legend>Are you OK? <span class="sr-only">(Please answer the question)</span></legend>
    <label><input type=radio />Yes</label>
    <label><input type=radio />No</label>
    <p class="error" aria-hidden="true">Please answer the question</p>
<fieldset>
```

### Client-side validation

As seen previously, form fields (`<input/>` tag) can have different types, like `email`, `url`, `date`, `number`, etc. and attributes like `required`. If the browser supports a given type or attribute, the UI will manage both input masking and error management. It will also display the adapted on-screen keyboard where appropriate.

Whenever the expected data match a type of input, it should be used. But not all types are equally supported across all browsers. So, the following strategy should be adopted when HTML5 input types or attributes are set:

* check browser support for each type or attribute (through a library like Modernizr)
* if not supported, provide a fallback to achieve similar behaviour

Different approaches can be adopted:

* inline, a.k.a. "on-the-fly" validation: when an error is detected on a field, the user is warned, and/or prompted for a resolution
* pre-submission validation: when the user submits the form, the browser checks the inputs before sending them to the server. The user is warned and prompted for a resolution
* a mix between the previous approaches: some fields are validated inline, and a pre-submission validation is performed. This approach may be needed when there are relationships between inputs. Example: the start date and end date are technically valid, but the end date is before the start date.

### Inline validation

The main issue here is to warn the non-visual user that a message is displayed. Even when the message is correctly associated with the field, it will be read only when the field receives focus, or through exploration. In some cases, when the programmatic association relies on the `aria-describedby` property, the help message is read with a delay; thus the user may have skipped the field too soon, and miss the notification. So this set of techniques is not reliable enough to be used alone.

To handle this, two features of the ARIA specification can be used:

* set an `aria-live` attribute on the message container
* set a `role=alert` on the message container

The `aria-live` attribute defines the element as a live region, meaning that the assistive technology will notify the user when changes occur in the element. Parameters can be used to control the assertiveness of the notification; the type of events that trigger the notification; and whether the whole element, or just the modified parts of it, must be output at notification.

The live region must have a container that is present at page load. A typical strategy is to have an empty container at page load, and to modify its text node dynamically, or append a child node.

Example 1: defines the notification as assertive, meaning it occurs immediately, interrupting the current flow.

At page load:

```html
<div aria-live="assertive"></div>
```

When the error occurs:

```html
<div aria-live="assertive"><p>Do something!</p></div>
```

Example 2: defines the notification as polite, meaning it occurs when the screen reader is idle for a few seconds.

At page load:

```html
<div aria-live="polite"></div>
```

When the error occurs:

```html
<div aria-live="polite"><p>Something happened, but it’s ok to handle it later</p></div>
```

Example 3: defines the error message block as "atomic", meaning the whole block is output to the user, regardless of which part of it has changed:

At page load:

```html
<div aria-live="assertive" aria-atomic="true"></div>
```

At first validation:

```html

<div aria-live="assertive" aria-atomic="true">
<h2>Errors in form</h2>
<ul>
  <li>The e-mail is required</li>
  <li>The birth date is invalid</li>
</ul>
</div>
```

The screen reader output will be something similar to:

> Heading, level 2. Errors in form
> Unordered list, 2 items. The e-mail is required. The birth date is invalid. End of list.

At second validation:

```html
<div aria-live="assertive" aria-atomic="true">
<h2>Errors in form</h2>
<ul>
  <li>The e-mail is invalid</li>
  <li>The birth date is invalid</li>
</ul>
</div>
```

The screen reader output will be something similar to:

> Heading, level 2. Errors in form
> Unordered list, 2 elements. The e-mail is invalid. The birth date is invalid. End of list.

Example 4: defines the error message block as "not atomic", meaning that only the modified parts are output to the user:

On page load:

```html
<div aria-live="assertive" aria-atomic="false"></div>
```

At first validation:

```html
<div aria-live="assertive" aria-atomic="false">
<h2>Errors in form</h2>
<ul>
  <li>The e-mail is required</li>
  <li>The birth date is invalid</li>
</ul>
</div>
```

The screen reader output will be something similar to:

> Heading, level 2. Errors in form
> Unordered list, 2 elements. The e-mail is required. The birth date is invalid. End of list.

At second validation:

```html
<div aria-live="assertive" aria-atomic="false">
<h2>Errors in form</h2>
<ul>
  <li>The e-mail is invalid</li>
  <li>The birth date is invalid</li>
</ul>
</div>
```

The screen reader output will be something similar to:

> The e-mail is invalid.

Depending on the form structure and features, one or the other solutions may be more adequate; only thorough user testing can determine which is best with a fair level of certainty.

An alternative way is to define the message container as an alert (`role="alert"`). It turns the element into a region with `aria-live="assertive"` and `aria-atomic="true"`.

It may not be desirable to validate every field on the fly. Non-visual users may explore the form before starting to fill it, in order to get an appreciation of the expected task. To do so, they will skip to each field in sequence, and possibly go back and forth. If validation occurs at each focus/blur event, the experience will be unpleasant. On the other hand, on very long forms, it may be preferable to help the user avoid the need to start again from the beginning. It’s a matter of balance.

In any case, validate the input on the server side.

## Custom interactive components

Custom interactive components (aka widgets) are HTML-based components that perform functions through additional scripting. Examples: drop-down menu, tab system, pop-up dialog box.

They oppose specific challenges to be made accessible:

* must be conveyed to assistive technologies:
** their accessible name 
** their accessible role 
** their changes of states and properties, if any
* they must be reachable and actionable with any type of input device (specifically: mouse, keyboard, touch, or devices with  interaction types similar to any of those)
* they must meet users expectations in terms of interaction schemes, when a consensus or a _de facto_ standard exist
* otherwise, instructions and hints on how to use the widget must be provided to the user, in a timely manner

To address those needs, HTML includes a set of attributes dedicated to communication with assistive technologies, in addition to general purpose features like text nodes and @title@ attributes. These attributes are described by the [WAI-ARIA specification](https://www.w3.org/TR/wai-aria-1.1/).

["Authoring best practices"](https://www.w3.org/TR/wai-aria-practices-1.1/) are available, to help design custom components that meet every requirement listed above. However, they are only guidelines, to inform developers of browsers, assistive technologies, and front-end code, in the hope that all tiers align their code in order to reach this goal. Strictly following the guidelines is therefore not a guarantee that a given piece of front-end code will "work" for users of assistive technologies. Such widgets must be tested with technologies used by the target audience, to verify that they are actually understandable and operable. 

The following sections describe best practices for specific components.


## Navigation menu button

### References

* https://inclusive-components.design/menus-menu-buttons/
* https://codepen.io/oliviernourry/pen/RjbdyO

Note: the [WAI-ARIA Authoring Practices 1.1](https://www.w3.org/TR/wai-aria-practices-1.1/) describes a design pattern named [Menu button](https://www.w3.org/TR/wai-aria-practices-1.1/#menubutton). It can be used for navigation menus, but is chiefly designed for app-like menus, with complex interactions and structure. There isn't a firm consensus on the suitability of this pattern for navigation. The pattern presented here aims at a simpler code and interaction scheme.

### Description

A menu button, when activated, displays a set of actionable items, links generally, named "menu". This menu (generally a list, or list of lists) can be dismissed and displayed by clicking on the button. A typical use of menu buttons is a "burger"-type menu, frequently found on web pages designed for mobile devices.

Note: the same pattern can be used for non-navigational menu buttons, when the widget is simple enough to not require the [W3C's Menu button design pattern](https://www.w3.org/TR/wai-aria-practices-1.1/#menubutton).

### Technical requirements

#### Keyboard interactions

* When the focus is on the button, and the Enter or the Space key is pressed, it toggles the visibility of the menu. The focus remains on the button
* When the focus is on the button and the menu is visible: pressing Tab key sets the focus on first item.
* When the focus is on the last item: pressing Tab closes the menu and sets the focus on first focusable item that follows.
* When the focus is in the menu: pressing Esc closes the menu and returns the focus to the button, without changing the previously selected option.
* When the focus is on the first item, pressing shift+Tab returns the focus on the button, and closes the menu.

#### Code structure

##### Button

The [Buttons](#buttons) section describes different techniques to meet the following requirements. 

* Must be an element that has a role of `button` (preferably, a `<button>` element, or an `<input type=button>` element).
* Must have an accessible name that conveys its exact purpose
* Can have an accessible description that completes the accessible name, if needed
* Must have an `aria-expanded` attribute, set to `true` if the menu is visible, and `false` if not
* Must have an `aria-controls` attribute, set to match the value of the `id` attribute of the menu container

##### Menu container

* Must be an element that has a role of `navigation` (preferably, a `<nav>` element)
* Must have a unique `id` attribute 
* Can contain a list (required if 2 elements or more), or a list of lists

##### Menu items

If an element is designed to appear visibly as "active", then this must be conveyed through text as well. Possible techniques (by descending order of preference):

* add a visually hidden text (through this [CSS-based technique](#hiding-content-from-view-not-to-screen-readers))
* add a `title` attribute to the link, following this pattern: `title="(link text) + (indication that it is active)"` (must be i18n-ready)
* add a `aria-label` attribute to the link, following this pattern: `aria-label="(link text) + (indication that it is active)"` (must be i18n-ready)



